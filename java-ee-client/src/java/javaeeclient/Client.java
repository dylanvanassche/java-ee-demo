package javaeeclient;


import boontjes.StatelessBoontjeRemote;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author student
 */
public class Client extends JFrame {
    private JButton submit = new JButton("SUBMIT");
    private JLabel huurprijs = new JLabel("€ ???");
    private JTextField aantalDagen = new JTextField();
    
    public Client(StatelessBoontjeRemote staatloos) {
        super();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().add(aantalDagen, BorderLayout.CENTER);
        this.getContentPane().add(huurprijs, BorderLayout.EAST);
        this.getContentPane().add(submit, BorderLayout.SOUTH);
        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { 
                double prijs = staatloos.huurprijs(Integer.parseInt(aantalDagen.getText()), 0.0);
                huurprijs.setText(new Double(prijs).toString());
              } 
        });
        this.pack();
        this.setVisible(true);
    }
}
