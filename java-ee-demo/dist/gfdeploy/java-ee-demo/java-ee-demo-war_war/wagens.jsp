<%-- 
    Document   : wagens
    Created on : 26-sep-2018, 15:16:39
    Author     : Dylan Van Assche
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reserveren</title>
    </head>
    <body>
        <%@include file="fragment.jspf" %>
        
        <h1>Welkom <c:out value="${sessionScope.klantennummer}" /></h1>
        <p>Reserveer uw wagen hieronder:</p>
        <form onsubmit="<c:url value='controller' />" method="post">
            <table>
                <tr>
                    <th>Pick-up location</th>
                    <th>Vehicle type</th>
                    <th>Drop-off location</th>
                    <th>Pick-up date</th>
                    <th>Drop-off date</th>
                    <th></th>
                </tr>
                <tr>
                    <td> 
                        <select>
                            <c:forEach var = "i" begin = "0" end = "${fn:length(applicationScope.locaties) - 1}">
                                <option value="${applicationScope.locaties[i]}"/><c:out value="${applicationScope.locaties[i]}"/></option>
                            </c:forEach>
                        </select> 
                    </td>
                    <td> 
                        <select>
                            <c:forEach var = "i" begin = "0" end = "${fn:length(applicationScope.autos) - 1}">
                                <option value="${applicationScope.autos[i]}"/><c:out value="${applicationScope.autos[i]}"/></option>
                            </c:forEach>
                        </select> 
                    </td>
                    <td> 
                        <select>
                            <c:forEach var = "i" begin = "0" end = "${fn:length(applicationScope.locaties) - 1}">
                                <option value="${applicationScope.locaties[i]}"/><c:out value="${applicationScope.locaties[i]}"/></option>
                            </c:forEach>
                        </select> 
                    </td>
                    <td><input type="date"/></td>
                    <td><input type="date"/></td>
                    <td><input type="text" pattern="[0-9]{1,3}" name="dagen"/></td>
                    <td><input type="submit" value="Reserveer" name="submit"></td>
                </tr>
            </table>
        </form>
                        
        <%@include file="pagina.jsp" %>
    </body>
</html>
