<%-- 
    Document   : registreer
    Created on : 3-okt-2018, 14:35:06
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registreren</title>
    </head>
    <body>
        <%@include file="fragment.jspf" %>
        
        <h1>Registreer</h1>
        <form method="post" action="<c:url value='controller' />">
            <label>Naam:</label><input type="text" name="naam"><br>
            <label>Adres:</label><input type="text" name="adres"><br>
            <label>Postcode:</label><input type="text" name="postcode" pattern="[0-9]{4}"><br>
            <label>Gemeente:</label><input type="text" name="gemeente"><br>
            <input type="submit" value="Geregistreerd" name="submit">
        </form>
            
        <%@include file="pagina.jsp" %>
    </body>
</html>
