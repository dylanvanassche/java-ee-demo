<%-- 
    Document   : pagina
    Created on : 3-okt-2018, 15:38:18
    Author     : student
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty initParam.email && not empty initParam.telefoon && not empty initParam.adres}">
            <address>
                Email: <c:out value="${initParam.email}" /><br>
                Telefoon: <c:out value="${initParam.telefoon}" /><br>
                Adres: <c:out value="${initParam.adres}" />
            </address>
</c:if>

<form>
    <input type="submit" value="Uitloggen" name="submit">
</form>

