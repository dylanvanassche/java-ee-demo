/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boontjes;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.Stateful;

/**
 *
 * @author student
 */
@Stateful
public class StatefullBoontje implements StatefullBoontjeRemote, Serializable {
    private double schatkist = 0.0;
    @EJB private StatelessBoontjeRemote staatloos;
    
    public double vulDeSchatkist(int aantalDagen, double korting) {
        double huurprijs = staatloos.huurprijs(aantalDagen, korting);
        schatkist = schatkist + huurprijs;
        return huurprijs;
    }
    
    public double getSchatkist() {
        return schatkist;
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
