/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boontjes;

import javax.ejb.Stateless;

/**
 *
 * @author student
 */
@Stateless
public class StatelessBoontje implements StatelessBoontjeRemote {

    @Override
    public double huurprijs(int aantalDagen, double korting) {
        double vasteDagPrijs = 15.0;
        double btw = 0.21;
        double prijs = aantalDagen * vasteDagPrijs * (1+btw);
        return prijs * (1-korting);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
