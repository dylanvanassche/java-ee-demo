<%-- 
    Document   : index.jsp
    Created on : 26-sep-2018, 14:45:28
    Author     : Dylan Van Assche
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vermeulen NV</title>
    </head>
    <body>
        <%@include file="fragment.jspf" %>
        
        <h1>Welkom</h1>
        <p>Geef hieronder je klantennummer in:</p>
        <form method="post" action="<c:url value='controller' />">
            <input type="text" name="klantennummer">
            <input type="submit" value="Reservatie" name="submit">
            <input type="submit" value="Registratie" name="submit">
        </form>
            
        <%@include file="pagina.jsp" %>
    </body>
</html>
