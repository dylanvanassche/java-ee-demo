<%-- 
    Document   : overzicht
    Created on : 3-okt-2018, 15:02:35
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Overzicht</title>
    </head>
    <body>
        <%@include file="fragment.jspf" %>
        <h1>Overzichtspagina voor <c:out value="${sessionScope.klantennummer}" /> </h1>
        Huurprijs: <c:out value="${sessionScope.huurprijs}" /> euro.
        Totaal: <c:out value="${sessionScope.totaal}" /> euro.
        <form method="post" action="<c:url value='controller' />">
            <input type="submit" value="Terug" name="submit">
        </form>
            
        <%@include file="pagina.jsp" %>
    </body>
</html>
