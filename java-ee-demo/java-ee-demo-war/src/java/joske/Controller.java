package joske;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import boontjes.StatefullBoontjeRemote;
import boontjes.StatelessBoontjeRemote;
import data.Locaties;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author student
 */
public class Controller extends HttpServlet {
    
    @EJB private StatelessBoontjeRemote staatsloos;
    @EJB private StatefullBoontjeRemote koning;
    
    private double kortingske;

    public void init() {
        Locaties loc = new Locaties();
        getServletContext().setAttribute("locaties", loc.getLocaties());
        getServletContext().setAttribute("autos", loc.getAutos());
        ServletConfig config = getServletConfig(); 
        getServletContext().setAttribute("korting", config.getInitParameter("korting"));
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String dispatcher = request.getParameter("submit");
        System.out.println("Dispatcher=" + dispatcher);
        switch(dispatcher) {
            case "Reservatie":
                String klantennummer = request.getParameter("klantennummer");
                System.out.println("Klantennummer=" + klantennummer);
                session.setAttribute("klantennummer", klantennummer); // enkel hier nummer zetten
            case "Geregistreerd":
            case "Terug":
                this.goToPage("wagens.jsp", request, response);
                break;
                
            case "Registratie":
                session.setAttribute("klantennummer", "123456789"); // enkel hier nummer zetten als dummy tot DB ready is
                this.goToPage("klant.jsp", request, response);
                break;
            
            case "Reserveer":
                int daagskes = Integer.parseInt(request.getParameter("dagen"));
                double huurprijs = koning.vulDeSchatkist(daagskes, kortingske);
                double totaal = koning.getSchatkist();
                session.setAttribute("huurprijs", huurprijs);
                session.setAttribute("totaal", totaal);
                this.goToPage("overzicht.jsp", request, response);
                break;
                
            case "Uitloggen":
                System.out.println("Uitloggen");
                session.invalidate();
                this.goToPage("index.jsp", request, response);
                break;
                
            default:
                System.out.println("Maat, je moet je dispatching fixen");
        }
        
    }
    
    private void goToPage(String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(page);
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
