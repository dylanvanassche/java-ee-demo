/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;

/**
 *
 * @author student
 */
public class Locaties {
    private ArrayList<String> plaatsnamen;
    private ArrayList<String> autos;
    
    public Locaties() {
        this.plaatsnamen = new ArrayList<String>();
        this.plaatsnamen.add("Vilvoorde");
        this.plaatsnamen.add("Sint-Katelijne-Waver");
        this.plaatsnamen.add("Meise is een dorp");
        this.plaatsnamen.add("Brussel");
        this.plaatsnamen.add("Zwevelgem");       
        
        this.autos = new ArrayList<String>();
        this.autos.add("Nissan Micra");
        this.autos.add("Bucht-Met-Wielen van de Laurens");
        this.autos.add("VW Golf");
        this.autos.add("Ne Ford is pas nen bak");
    }
    
    public ArrayList<String> getLocaties() {
        return this.plaatsnamen;
    }
    
    public ArrayList<String> getAutos() {
        return this.autos;
    }
}
