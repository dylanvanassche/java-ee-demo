/**
 * Author:  Dylan Van Assche
 * Created: 10-okt-2018
 * Joske maakt tabellen aan 
 */

-- Drop if needed, reverse order of dependencies creation
DROP TABLE reservaties PURGE;
DROP TABLE klanten PURGE;
DROP TABLE wagens PURGE;
DROP TABLE locaties PURGE;

-- Create required tables
CREATE TABLE klanten(
    knr INT,
    knaam VARCHAR2(50),
    adres VARCHAR2(100),
    postcode INT,
    gemeente VARCHAR2(50),
    PRIMARY KEY(knr)
);

CREATE TABLE wagens(
    wnr INT,
    wnaam VARCHAR2(50),
    prijs FLOAT,
    PRIMARY KEY(wnr)
);

CREATE TABLE locaties(
    lnr INT,
    lnaam VARCHAR2(50),
    PRIMARY KEY(lnr)
);

CREATE TABLE reservaties(
    mr INT,
    datumres DATE,
    dagen INT,
    datumvan DATE,
    knr INT,
    wnr INT,
    lnr INT,
	lnaam VARCHAR2(50),
    PRIMARY KEY(mr),
    FOREIGN KEY(knr) REFERENCES klanten(knr),
    FOREIGN KEY(wnr) REFERENCES wagens(wnr),
    FOREIGN KEY(lnr) REFERENCES locaties(lnr),
	FOREIGN KEY(lnaam) REFERENCES locaties(lnaam)
);

-- Klantjes
INSERT INTO klanten (knr, knaam, adres, postcode, gemeente) VALUES (1, 'Joske Vermeulen', 'Konijnenweg 69', 1877, 'Kontich');

-- Plaatskes
INSERT INTO locaties (lnr, lnaam) VALUES (1, 'Vilvoorde');
INSERT INTO locaties (lnr, lnaam) VALUES (2, 'Sint-Katelijne-Waver');
INSERT INTO locaties (lnr, lnaam) VALUES (3, 'Meise is een dorp');
INSERT INTO locaties (lnr, lnaam) VALUES (4, 'Brussel');
INSERT INTO locaties (lnr, lnaam) VALUES (5, 'Zwevelgem');

-- Autookes
INSERT INTO wagens (wnr, wnaam, prijs) VALUES (1, 'Nissan Micra', 12058.74);
INSERT INTO wagens (wnr, wnaam, prijs) VALUES (2, 'Bucht-Met-Wielen van Laurens', 4006.42);
INSERT INTO wagens (wnr, wnaam, prijs) VALUES (3, 'VW Golf', 10048.44);
INSERT INTO wagens (wnr, wnaam, prijs) VALUES (4, 'Ne Ford, das pas nen bak', 9047.55);

-- Reserveringen
INSERT INTO reservaties (mr, datumres, dagen, datumvan, knr, wnr, lnr, lnaam) VALUES (1, TO_DATE('2018/10/10 15:02:44', 'yyyy/mm/dd hh24:mi:ss'), 5, TO_DATE('2018/10/10 16:02:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 1, 1, 'Vilvoorde');
