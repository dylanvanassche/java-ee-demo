/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boontjes;

import javax.ejb.Remote;

/**
 *
 * @author student
 */
@Remote
public interface StatelessBoontjeRemote {
    double huurprijs(int aantalDagen, double korting);
}
